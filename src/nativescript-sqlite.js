"use strict";
/**
 * This removes the nativescript-sqlite code from the WEB version of the project.
 *
 * This src folder is invisible to the native script build as it uses the app folder,
 * symlinked to the src parent.
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = false;
//# sourceMappingURL=nativescript-sqlite.js.map