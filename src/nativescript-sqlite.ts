/**
 * This removes the nativescript-sqlite code from the WEB version of the project.
 *
 * This src folder is invisible to the native script build as it uses the app folder,
 * symlinked to the src parent.
 *
 */

export default false;
