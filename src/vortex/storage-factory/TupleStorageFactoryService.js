"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TupleStorageFactoryService = /** @class */ (function () {
    function TupleStorageFactoryService(webSqlFactory) {
        this.webSqlFactory = webSqlFactory;
    }
    return TupleStorageFactoryService;
}());
exports.TupleStorageFactoryService = TupleStorageFactoryService;
//# sourceMappingURL=TupleStorageFactoryService.js.map