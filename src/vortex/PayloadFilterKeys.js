"use strict";
/**
 * The file defines some commonly used filter keys
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.rapuiServerEcho = "rapuiServerEcho";
exports.rapuiClientEcho = "rapuiClientEcho";
exports.rapuiVortexUuid = "rapuiVortexUuid";
exports.plIdKey = "id";
exports.plDeleteKey = "delete";
//# sourceMappingURL=PayloadFilterKeys.js.map