/**
 * The file defines some commonly used filter keys
 */
export declare let rapuiServerEcho: string;
export declare let rapuiClientEcho: string;
export declare let rapuiVortexUuid: string;
export declare let plIdKey: string;
export declare let plDeleteKey: string;
