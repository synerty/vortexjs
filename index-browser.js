"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PayloadDelegateWeb_1 = require("./src/vortex/payload/PayloadDelegateWeb");
exports.PayloadDelegateWeb = PayloadDelegateWeb_1.PayloadDelegateWeb;
var WebSqlBrowserAdaptorService_1 = require("./src/websql/WebSqlBrowserAdaptorService");
exports.WebSqlBrowserFactoryService = WebSqlBrowserAdaptorService_1.WebSqlBrowserFactoryService;
var TupleStorageFactoryServiceWeb_1 = require("./src/vortex/storage-factory/TupleStorageFactoryServiceWeb");
exports.TupleStorageFactoryServiceWeb = TupleStorageFactoryServiceWeb_1.TupleStorageFactoryServiceWeb;
//# sourceMappingURL=index-browser.js.map