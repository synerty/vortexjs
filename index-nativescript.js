"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PayloadDelegateNs_1 = require("./src/vortex/payload/PayloadDelegateNs");
exports.PayloadDelegateNs = PayloadDelegateNs_1.PayloadDelegateNs;
var WebSqlNativeScriptAdaptorService_1 = require("./src/websql/WebSqlNativeScriptAdaptorService");
exports.WebSqlNativeScriptFactoryService = WebSqlNativeScriptAdaptorService_1.WebSqlNativeScriptFactoryService;
var WebSqlNativeScriptThreadedAdaptorService_1 = require("./src/websql/WebSqlNativeScriptThreadedAdaptorService");
exports.WebSqlNativeScriptThreadedFactoryService = WebSqlNativeScriptThreadedAdaptorService_1.WebSqlNativeScriptThreadedFactoryService;
var TupleStorageFactoryServiceNs_1 = require("./src/vortex/storage-factory/TupleStorageFactoryServiceNs");
exports.TupleStorageFactoryServiceNs = TupleStorageFactoryServiceNs_1.TupleStorageFactoryServiceNs;
//# sourceMappingURL=index-nativescript.js.map