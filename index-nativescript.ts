export {PayloadDelegateNs} from "./src/vortex/payload/PayloadDelegateNs";

export {WebSqlNativeScriptFactoryService} from "./src/websql/WebSqlNativeScriptAdaptorService";
export {WebSqlNativeScriptThreadedFactoryService} from "./src/websql/WebSqlNativeScriptThreadedAdaptorService";

export {
    TupleStorageFactoryServiceNs
} from "./src/vortex/storage-factory/TupleStorageFactoryServiceNs";

