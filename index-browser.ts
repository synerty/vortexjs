export {PayloadDelegateWeb} from "./src/vortex/payload/PayloadDelegateWeb";

export {WebSqlBrowserFactoryService} from "./src/websql/WebSqlBrowserAdaptorService";

export {
    TupleStorageFactoryServiceWeb
} from "./src/vortex/storage-factory/TupleStorageFactoryServiceWeb";
